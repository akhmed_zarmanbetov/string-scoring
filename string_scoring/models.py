from typing import List, Optional
from pydantic import BaseModel, Field, validator


class PairString(BaseModel):
    id: int
    string1: str = Field(
        max_length=20, 
        description='String with length less then 20.',
        example='Ahmed',
    )
    string2: str = Field(
        max_length=20, 
        description='String with length less then 20.',
        example='ahmedushka',
    )


class ResponsePairString(BaseModel):
    id: int
    score: int


class User(BaseModel):
    first_name: str
    last_name: str
    address: str
    zip: int = 100000

    @validator('zip')
    def val_length(cls, v: int) -> int:
        if len(str(v)) != 6:
            raise ValueError('zip length must be equal 6!')
        return v

    @validator('first_name', 'last_name', 'address')
    def lower(cls, v: str) -> str:
        return v.lower()

    def concat(self) -> str:
        return f'{self.first_name} {self.last_name} {self.address}'


class RequestScoring(BaseModel):
    user: User
    candidates: List[User]
    limit: int = 5


class ResponseUser(BaseModel):
    concat: str
    score: int


class ResponseScoring(BaseModel):
    users: List[ResponseUser]
