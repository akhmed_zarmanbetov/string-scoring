from fastapi import FastAPI
from thefuzz import fuzz, process

from string_scoring.models import (PairString, RequestScoring, ResponseUser,
                                   ResponsePairString, ResponseScoring)

app = FastAPI()


@app.post("/two_string", response_model=ResponsePairString)
async def two_string(strings: PairString):
    score = fuzz.ratio(strings.string1, strings.string2)
    response = ResponsePairString(
        id=strings.id,
        score=score,
    )

    return response


@app.post("/scoring", response_model=ResponseScoring)
async def scoring(request: RequestScoring):
    user = request.user.concat()
    candidates = [candidate.concat() for candidate in request.candidates]

    result = process.extract(
        query=user,
        choices=candidates,
        limit=request.limit,
    )

    response_users = [ResponseUser(concat=concat, score=score) 
                      for concat, score in result]

    return ResponseScoring(users=response_users)
